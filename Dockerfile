FROM rocker/r-ver:4.3.1@sha256:74556f2c53fb0eb788198a902c169f4aee10d033f3d8304122fc0564766d2794

RUN apt-get update \
  && apt-get install --yes --no-install-recommends \
  git=1:2.34.1-1ubuntu1.11 \
  libssl-dev=3.0.2-0ubuntu1.18 \
  libcurl4-openssl-dev=7.81.0-1ubuntu1.20 \
  curl=7.81.0-1ubuntu1.20 \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
  && curl -O https://cran.r-project.org/src/contrib/Archive/remotes/remotes_2.4.2.1.tar.gz

RUN R -e "install.packages('remotes_2.4.2.1.tar.gz'); \
	remotes::install_version('stringr', '1.4.1', repos='https://packagemanager.posit.co/cran/2024-01-12', upgrade=FALSE); \
	remotes::install_version('dplyr','1.1.0', repos='https://packagemanager.posit.co/cran/2024-01-12', upgrade=FALSE ); \
	remotes::install_version('tidyr','1.1.0', repos='https://packagemanager.posit.co/cran/2024-01-12', upgrade=FALSE); \
	remotes::install_version('readr','2.1.3', repos='https://packagemanager.posit.co/cran/2024-01-12', upgrade=FALSE)" && \
    rm -rf /tmp/*
